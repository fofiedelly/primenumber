/**
* with love with Sublime text
* return the sum of the prime number up to a given number
* Implemented with the dynamic method of Erastosthenes
* @author Delly Fofie - fofiedell@gmail.com
* == > https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
*/
#include "stdafx.h"
#include <iostream>
#include <vector>

using namespace std;

std::vector<int> generate_primes(int n)
{
	std::vector<bool>sieve(n, true);
	for (std::size_t i = 2; i < sieve.size(); ++i) 
	{
		if (sieve[i])
		{
			for (auto j = i*i; j < sieve.size(); j += i)
			{
				sieve[j] = false;
			}
		}
	}

	std::vector<int> primes;
	for (std::size_t i = 2; i < sieve.size(); ++i)
		if (sieve[i])
		{
			primes.push_back(i);
		}
	return primes;
}

int main()
{
	std::vector<int> result = generate_primes(2000000);
	int sum = 0;
	for (int i = 0; i < result.size(); i++)
	{
		sum += result[i];
	}
	int stop = 0;
	cin >> stop;
    return 0;
}


